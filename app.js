const home = document.getElementById('home');
const projects = document.getElementById('projects');
const about = document.getElementById('about');
const contact = document.getElementById('contact');


function getActiveElement(){
    if(home.classList.value === "active")
        return home;
    else if(projects.classList.value === "active")
        return projects;
    else if(about.classList.value === "active")
        return about;
    else if(contact.classList.value === "active")
        return contact;
}


projects.addEventListener('click',(event) => {
    let activeElement = getActiveElement();
    activeElement.classList.remove('active');
    projects.classList = 'active';
});

about.addEventListener('click',(event) => {
    let activeElement = getActiveElement();
    activeElement.classList.remove('active');
    about.classList = 'active';
});

contact.addEventListener('click',(event) => {
    let activeElement = getActiveElement();
    activeElement.classList.remove('active');
    contact.classList = 'active';
});

home.addEventListener('click',(event) => {
    let activeElement = getActiveElement();
    activeElement.classList.remove('active');
    home.classList = 'active';
});